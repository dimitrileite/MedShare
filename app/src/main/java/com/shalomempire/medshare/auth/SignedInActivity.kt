/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shalomempire.medshare.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.firebase.ui.auth.util.ExtraConstants
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.*
import com.shalomempire.medshare.R
import com.shalomempire.medshare.storage.GlideApp
import kotlinx.android.synthetic.main.signed_in_layout.*
import java.util.*

class SignedInActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signed_in_layout)

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            startActivity(AuthUiActivity.createIntent(this))
            finish()
            return

        }

        val response = intent.getParcelableExtra<IdpResponse>(ExtraConstants.IDP_RESPONSE)

        populateProfile(response)
        populateIdpToken(response)

        sign_out.setOnClickListener(this)
        delete_account.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.sign_out -> signOut()
            R.id.delete_account -> deleteAccountClicked()
        }
    }

    private fun signOut() {
        AuthUI.getInstance()
            .signOut(this)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    startActivity(AuthUiActivity.createIntent(this@SignedInActivity))
                    finish()
                } else {
                    Log.w(TAG, "signOut:failure", task.exception)
                    showSnackbar(R.string.sign_out_failed)
                }
            }
    }

    private fun deleteAccountClicked() {
        AlertDialog.Builder(this@SignedInActivity)
            .setMessage("Are you sure you want to delete this account?")
            .setPositiveButton("Yes, nuke it!") { dialog, which -> deleteAccount() }
            .setNegativeButton("No") { dialog, which -> null }
            .show()
    }

    private fun deleteAccount() {
        AuthUI.getInstance()
            .delete(this)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    startActivity(AuthUiActivity.createIntent(this@SignedInActivity))
                    finish()
                } else {
                    showSnackbar(R.string.delete_account_failed)
                }
            }
    }

    private fun populateProfile(response: IdpResponse?) {
        val user = FirebaseAuth.getInstance().currentUser
        if (user!!.photoUrl != null) {

            GlideApp.with(this)
                .load(user.photoUrl)
                .fitCenter()
                .into(user_profile_picture)
            /*Glide.with(this)
                .load(user.photoUrl)
                .into(user_profile_picture!!)*/
        }

        user_email.text = if (TextUtils.isEmpty(user.email)) "No email" else user.email
        user_phone_number.text =
                if (TextUtils.isEmpty(user.phoneNumber)) "No phone number" else user.phoneNumber
        user_display_name.text =
                if (TextUtils.isEmpty(user.displayName)) "No display name" else user.displayName

        if (response == null) {
            user_is_new.visibility = View.GONE
        } else {
            user_is_new.visibility = View.VISIBLE
            user_is_new.text = if (response.isNewUser) "New user" else "Existing user"
        }

        val providers = ArrayList<String>()
        if (user.providerData.isEmpty()) {
            providers.add(getString(R.string.providers_anonymous))
        } else {
            for (info in user.providerData) {
                when (info.providerId) {
                    GoogleAuthProvider.PROVIDER_ID -> providers.add(getString(R.string.providers_google))
                    FacebookAuthProvider.PROVIDER_ID -> providers.add(getString(R.string.providers_facebook))
                    TwitterAuthProvider.PROVIDER_ID -> providers.add(getString(R.string.providers_twitter))
                    GithubAuthProvider.PROVIDER_ID -> providers.add(getString(R.string.providers_github))
                    EmailAuthProvider.PROVIDER_ID -> providers.add(getString(R.string.providers_email))
                    PhoneAuthProvider.PROVIDER_ID -> providers.add(getString(R.string.providers_phone))
                    FirebaseAuthProvider.PROVIDER_ID -> {
                    }
                    else -> throw IllegalStateException(
                        "Unknown provider: " + info.providerId
                    )
                }// Ignore this provider, it's not very meaningful
            }
        }

        user_enabled_providers.text = getString(R.string.used_providers, providers)
    }

    private fun populateIdpToken(response: IdpResponse?) {
        var token: String? = null
        var secret: String? = null
        if (response != null) {
            token = response.idpToken
            secret = response.idpSecret
        }

        if (token == null) {
            idp_token_layout.visibility = View.GONE
        } else {
            idp_token_layout.visibility = View.VISIBLE
            idp_token.text = token
        }


        if (secret == null) {
            idp_secret_layout.visibility = View.GONE
        } else {
            idp_secret_layout.visibility = View.VISIBLE
            idp_secret.text = secret
        }
    }

    private fun showSnackbar(@StringRes errorMessageRes: Int) {
        Snackbar.make(root, errorMessageRes, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        private const val TAG = "SignedInActivity"

        fun createIntent(context: Context, response: IdpResponse?): Intent {
            return Intent().setClass(context, SignedInActivity::class.java)
                .putExtra(ExtraConstants.IDP_RESPONSE, response)
        }
    }
}
