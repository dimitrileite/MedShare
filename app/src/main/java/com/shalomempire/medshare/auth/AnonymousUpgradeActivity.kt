package com.shalomempire.medshare.auth

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.shalomempire.medshare.R
import com.shalomempire.medshare.util.ConfigurationUtils
import kotlinx.android.synthetic.main.activity_anonymous_upgrade.*


class AnonymousUpgradeActivity : AppCompatActivity(), View.OnClickListener {

    private var mPendingCredential: AuthCredential? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anonymous_upgrade)

        anon_sign_in.setOnClickListener(this)
        begin_flow.setOnClickListener(this)
        resolve_merge.setOnClickListener(this)
        sign_out.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.anon_sign_in -> signInAnonymously()
            R.id.begin_flow -> startAuthUI()
            R.id.resolve_merge -> resolveMerge()
            R.id.sign_out -> signOut()
        }
    }

    private fun signInAnonymously() {
        FirebaseAuth.getInstance().signInAnonymously()
            .addOnCompleteListener(
                this
            ) { task ->
                updateUI()

                if (task.isSuccessful) {
                    setStatus("Signed in anonymously as user " + getUserIdentifier(task.result!!.user))
                } else {
                    setStatus("Anonymous sign in failed.")
                }
            }
    }

    private fun startAuthUI() {
        val providers = ConfigurationUtils.getConfiguredProviders(this)
        val intent = AuthUI.getInstance().createSignInIntentBuilder()
            .setLogo(R.drawable.firebase_auth_120dp)
            .setAvailableProviders(providers)
            .enableAnonymousUsersAutoUpgrade()
            .build()
        startActivityForResult(intent, RC_SIGN_IN)
    }

    private fun resolveMerge() {
        if (mPendingCredential == null) {
            Toast.makeText(this, "Nothing to resolve.", Toast.LENGTH_SHORT).show()
            return
        }

        // TODO: Show how to do good data moving
        FirebaseAuth.getInstance().signInWithCredential(mPendingCredential!!)
            .addOnCompleteListener(this) { task ->
                mPendingCredential!! == null
                updateUI()

                if (task.isSuccessful) {
                    setStatus("Signed in as " + getUserIdentifier(task.result!!.user))
                } else {
                    Log.w(TAG, "Merge failed", task.exception)
                    setStatus("Failed to resolve merge conflict, see logs.")
                }
            }
    }

    private fun signOut() {
        AuthUI.getInstance().signOut(this)
            .addOnCompleteListener {
                setStatus(null)
                updateUI()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)
                ?: // User pressed back button
                return
            when {
                resultCode == RESULT_OK -> setStatus("Signed in as " + getUserIdentifier(FirebaseAuth.getInstance().currentUser!!))
                response.error!!.errorCode == ErrorCodes.ANONYMOUS_UPGRADE_MERGE_CONFLICT -> {

                    setStatus("Merge conflict: user already exists.")
                    resolve_merge.isEnabled = true
                    mPendingCredential = response.credentialForLinking!!

                }
                else -> {
                    Toast.makeText(this, "Auth error, see logs", Toast.LENGTH_SHORT).show()
                    Log.w(TAG, "Error: " + response.error!!.message, response.error)
                }
            }

            updateUI()
        }
    }

    private fun updateUI() {
        val currentUser = FirebaseAuth.getInstance().currentUser

        if (currentUser == null) {
            // Not signed in
            anon_sign_in.isEnabled = true
            begin_flow.isEnabled = false
            resolve_merge.isEnabled = false
            sign_out.isEnabled = false
        } else if (mPendingCredential == null && currentUser.isAnonymous) {
            // Anonymous user, waiting for linking
            anon_sign_in.isEnabled = false
            begin_flow.isEnabled = true
            resolve_merge.isEnabled = false
            sign_out.isEnabled = true
        } else if (mPendingCredential == null && !currentUser.isAnonymous) {
            // Fully signed in
            anon_sign_in.isEnabled = false
            begin_flow.isEnabled = false
            resolve_merge.isEnabled = false
            sign_out.isEnabled = true
        } else if (mPendingCredential != null) {
            // Signed in anonymous, awaiting merge conflict
            anon_sign_in.isEnabled = false
            begin_flow.isEnabled = false
            resolve_merge.isEnabled = true
            sign_out.isEnabled = true
        }
    }

    private fun setStatus(message: String?) {
        status_text.text = message
    }

    private fun getUserIdentifier(user: FirebaseUser): String? {
        return if (user.isAnonymous) {
            user.uid
        } else if (!TextUtils.isEmpty(user.email)) {
            user.email
        } else if (!TextUtils.isEmpty(user.phoneNumber)) {
            user.phoneNumber
        } else {
            "unknown"
        }
    }

    companion object {

        private const val TAG = "AccountLink"

        private const val RC_SIGN_IN = 123
    }
}
