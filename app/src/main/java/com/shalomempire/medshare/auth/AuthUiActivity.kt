/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shalomempire.medshare.auth

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.AuthUI.IdpConfig
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.android.gms.common.Scopes
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.shalomempire.medshare.R
import com.shalomempire.medshare.util.ConfigurationUtils
import kotlinx.android.synthetic.main.auth_ui_layout.*
import java.util.*

class AuthUiActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mDefaultTheme: RadioButton // R.id.default_theme
    private lateinit var mNoLogo: RadioButton // R.id.no_logo

    private val selectedTheme: Int
        @StyleRes
        get() {
            if (green_theme.isChecked) return R.style.GreenTheme
            else if (purple_theme.isChecked) return R.style.PurpleTheme
            return AuthUI.getDefaultTheme()
        }

    private val selectedLogo: Int
        @DrawableRes
        get() {
            if (firebase_logo.isChecked) return R.drawable.firebase_auth_120dp
            else if (google_logo.isChecked) return R.drawable.ic_googleg_color_144dp
            return AuthUI.NO_LOGO
        }

    private val selectedProviders: List<IdpConfig>
        get() {
            val selectedProviders = ArrayList<IdpConfig>()

            if (google_provider.isChecked) {
                selectedProviders.add(
                    IdpConfig.GoogleBuilder()
                        .setScopes(googleScopes)
                        .build()
                )
            }

            if (facebook_provider.isChecked) {
                selectedProviders.add(
                    IdpConfig.FacebookBuilder()
                        .setPermissions(facebookPermissions)
                        .build()
                )
            }

            if (twitter_provider.isChecked) {
                selectedProviders.add(IdpConfig.TwitterBuilder().build())
            }

            if (github_provider.isChecked) {
                selectedProviders.add(
                    IdpConfig.GitHubBuilder()
                        .setPermissions(gitHubPermissions)
                        .build()
                )
            }

            if (email_provider.isChecked) {
                selectedProviders.add(
                    IdpConfig.EmailBuilder()
                        .setRequireName(require_name.isChecked)
                        .setAllowNewAccounts(allow_new_email_accounts.isChecked)
                        .build()
                )
            }

            if (phone_provider.isChecked) {
                selectedProviders.add(IdpConfig.PhoneBuilder().build())
            }

            if (anonymous_provider.isChecked) {
                selectedProviders.add(IdpConfig.AnonymousBuilder().build())
            }

            return selectedProviders
        }

    private val selectedTosUrl: String?
        get() {
            if (google_tos_privacy.isChecked) {
                return GOOGLE_TOS_URL
            }

            return if (firebase_tos_privacy.isChecked) {
                FIREBASE_TOS_URL
            } else null

        }

    private val selectedPrivacyPolicyUrl: String?
        get() {
            if (google_tos_privacy.isChecked) {
                return GOOGLE_PRIVACY_POLICY_URL
            }

            return if (firebase_tos_privacy.isChecked) {
                FIREBASE_PRIVACY_POLICY_URL
            } else null

        }

    private val googleScopes: List<String>
        get() {
            val result = ArrayList<String>()
            if (google_scope_youtube_data.isChecked) {
                result.add("https://www.googleapis.com/auth/youtube.readonly")
            }
            if (google_scope_drive_file.isChecked) {
                result.add(Scopes.DRIVE_FILE)
            }
            return result
        }

    private val facebookPermissions: List<String>
        get() {
            val result = ArrayList<String>()
            if (facebook_permission_friends.isChecked) {
                result.add("user_friends")
            }
            if (facebook_permission_photos.isChecked) {
                result.add("user_photos")
            }
            return result
        }

    private val gitHubPermissions: List<String>
        get() {
            val result = ArrayList<String>()
            if (github_permission_repo.isChecked) {
                result.add("repo")
            }
            if (github_permission_gist.isChecked) {
                result.add("gist")
            }
            return result
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.auth_ui_layout)

        if (ConfigurationUtils.isGoogleMisconfigured(this)) {
            google_provider.isChecked = false
            google_provider.isEnabled = false
            google_provider.setText(R.string.google_label_missing_config)
            setGoogleScopesEnabled(false)
        } else {
            setGoogleScopesEnabled(google_provider.isChecked)
            google_provider.setOnCheckedChangeListener { compoundButton, checked -> setGoogleScopesEnabled(checked) }
        }

        if (ConfigurationUtils.isFacebookMisconfigured(this)) {
            facebook_provider.isChecked = false
            facebook_provider.isEnabled = false
            facebook_provider.setText(R.string.facebook_label_missing_config)
            setFacebookPermissionsEnabled(false)
        } else {
            setFacebookPermissionsEnabled(facebook_provider.isChecked)
            facebook_provider.setOnCheckedChangeListener { compoundButton, checked ->
                setFacebookPermissionsEnabled(
                    checked
                )
            }
        }

        if (ConfigurationUtils.isTwitterMisconfigured(this)) {
            twitter_provider.isChecked = false
            twitter_provider.isEnabled = false
            twitter_provider.setText(R.string.twitter_label_missing_config)
        }

        if (ConfigurationUtils.isGitHubMisconfigured(this)) {
            github_provider.isChecked = false
            github_provider.isEnabled = false
            github_provider.setText(R.string.github_label_missing_config)
            setGitHubPermissionsEnabled(false)
        } else {
            setGitHubPermissionsEnabled(github_provider.isChecked)
            github_provider.setOnCheckedChangeListener { compoundButton, checked ->
                setGitHubPermissionsEnabled(
                    checked
                )
            }
        }

        if (ConfigurationUtils.isGoogleMisconfigured(this)
            || ConfigurationUtils.isFacebookMisconfigured(this)
            || ConfigurationUtils.isTwitterMisconfigured(this)
            || ConfigurationUtils.isGitHubMisconfigured(this)
        ) {
            showSnackbar(R.string.configuration_required)
        }

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            dark_theme.isChecked = true
        }

        sign_in.setOnClickListener(this)
        sign_in_silent.setOnClickListener(this)
        default_theme.setOnClickListener(this)
        purple_theme.setOnClickListener(this)
        green_theme.setOnClickListener(this)
        dark_theme.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.sign_in -> signIn()
            R.id.sign_in_silent -> silentSignIn()
            R.id.default_theme, R.id.purple_theme, R.id.green_theme, R.id.dark_theme -> toggleDarkTheme()
        }
    }



    private fun signIn() {
        val builder = AuthUI.getInstance().createSignInIntentBuilder()
            .setTheme(selectedTheme)
            .setLogo(selectedLogo)
            .setAvailableProviders(selectedProviders)
            .setIsSmartLockEnabled(
                credential_selector_enabled.isChecked,
                hint_selector_enabled.isChecked
            )

        if (selectedTosUrl != null && selectedPrivacyPolicyUrl != null) {
            builder.setTosAndPrivacyPolicyUrls(
                selectedTosUrl!!,
                selectedPrivacyPolicyUrl!!
            )
        }

        startActivityForResult(builder.build(), RC_SIGN_IN)
    }

    private fun silentSignIn() {
        AuthUI.getInstance().silentSignIn(this, selectedProviders)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    startSignedInActivity(null)
                } else {
                    showSnackbar(R.string.sign_in_failed)
                }
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            handleSignInResponse(resultCode, data)
        }
    }

    override fun onResume() {
        super.onResume()
        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null) {
            startSignedInActivity(null)
            finish()
        }
    }

    private fun handleSignInResponse(resultCode: Int, data: Intent?) {
        val response: IdpResponse? = IdpResponse.fromResultIntent(data)
        // Successfully signed in
        if (resultCode == RESULT_OK) {
            startSignedInActivity(response)
            finish()
        } else {
            // Sign in failed
            if (response == null) {
                // User pressed back button
                showSnackbar(R.string.sign_in_cancelled)
                return
            }
            if (response.error!!.errorCode == ErrorCodes.NO_NETWORK) {
                showSnackbar(R.string.no_internet_connection)
                return
            }
            showSnackbar(R.string.unknown_error)
            Log.e(TAG, "Sign-in error: ", response.error)
        }
    }

    private fun startSignedInActivity(response: IdpResponse?) {
        startActivity(SignedInActivity.createIntent(this, response))
    }

    private fun toggleDarkTheme() {
        val mode = if (dark_theme.isChecked)
            AppCompatDelegate.MODE_NIGHT_YES
        else
            AppCompatDelegate.MODE_NIGHT_AUTO
        AppCompatDelegate.setDefaultNightMode(mode)
        delegate.setLocalNightMode(mode)
    }

    private fun setGoogleScopesEnabled(enabled: Boolean) {
        google_scopes_header.isEnabled = enabled
        google_scope_drive_file.isEnabled = enabled
        google_scope_youtube_data.isEnabled = enabled
    }

    private fun setFacebookPermissionsEnabled(enabled: Boolean) {
        facebook_permissions_header.isEnabled = enabled
        facebook_permission_friends.isEnabled = enabled
        facebook_permission_photos.isEnabled = enabled
    }

    private fun setGitHubPermissionsEnabled(enabled: Boolean) {
        github_permissions_header.isEnabled = enabled
        github_permission_repo.isEnabled = enabled
        github_permission_gist.isEnabled = enabled
    }

    private fun showSnackbar(@StringRes errorMessageRes: Int) {
        Snackbar.make(root, errorMessageRes, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        private const val TAG = "AuthUiActivity"

        private const val GOOGLE_TOS_URL = "https://www.google.com/policies/terms/"
        private const val FIREBASE_TOS_URL = "https://firebase.google.com/terms/"
        private const val GOOGLE_PRIVACY_POLICY_URL = "https://www.google.com/policies/privacy/"
        private const val FIREBASE_PRIVACY_POLICY_URL = "https://firebase.google.com/terms/analytics/#7_privacy"

        private const val RC_SIGN_IN = 100

        fun createIntent(context: Context): Intent {
            return Intent(context, AuthUiActivity::class.java)
        }
    }
}
