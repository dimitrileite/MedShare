package com.shalomempire.medshare.storage

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.shalomempire.medshare.BuildConfig
import com.shalomempire.medshare.R
import com.shalomempire.medshare.util.SignInResultNotifier
import kotlinx.android.synthetic.main.activity_image.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.util.*

open class ImageActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks, View.OnClickListener {

    private var mImageRef: StorageReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        // By default, Cloud Storage files require authentication to read or write.
        // For this sample to function correctly, enable Anonymous Auth in the Firebase console:
        // https://console.firebase.google.com/project/_/authentication/providers
        FirebaseAuth.getInstance()
            .signInAnonymously()
            .addOnCompleteListener(SignInResultNotifier(this))

        button_choose_photo.setOnClickListener(this)
        button_download_direct.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.button_choose_photo -> choosePhoto()
            R.id.button_download_direct -> downloadDirect()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_CHOOSE_PHOTO) {
            if (resultCode == RESULT_OK) {
                val selectedImage = data!!.data
                uploadPhoto(selectedImage)
            } else {
                Toast.makeText(this, "No image chosen", Toast.LENGTH_SHORT).show()
            }
        } else if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE && EasyPermissions.hasPermissions(
                this,
                PERMS
            )
        ) {
            choosePhoto()
        }
    }

    @AfterPermissionGranted(RC_IMAGE_PERMS)
    protected fun choosePhoto() {
        if (!EasyPermissions.hasPermissions(this, PERMS)) {
            EasyPermissions.requestPermissions(
                this, getString(R.string.rational_image_perm),
                RC_IMAGE_PERMS, PERMS
            )
            return
        }

        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(i, RC_CHOOSE_PHOTO)
    }

    private fun uploadPhoto(uri: Uri?) {
        // Reset UI
        hideDownloadUI()
        Toast.makeText(this, "Uploading...", Toast.LENGTH_SHORT).show()

        // Upload to Cloud Storage
        val uuid = UUID.randomUUID().toString()
        mImageRef = FirebaseStorage.getInstance().getReference(uuid)
        mImageRef!!.putFile(uri!!)
            .addOnSuccessListener(
                this
            ) { taskSnapshot ->
                if (BuildConfig.DEBUG) {
                    Log.d(TAG, "uploadPhoto:onSuccess:" + taskSnapshot.metadata!!.reference!!.path)
                }
                Toast.makeText(
                    this@ImageActivity, "Image uploaded",
                    Toast.LENGTH_SHORT
                ).show()

                showDownloadUI()
            }
            .addOnFailureListener(this) { e ->
                Log.w(TAG, "uploadPhoto:onError", e)
                Toast.makeText(
                    this@ImageActivity, "Upload failed",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    private fun downloadDirect() {
        // Download directly from StorageReference using Glide
        // (See MyAppGlideModule for Loader registration)
        GlideApp.with(this)
            .load(mImageRef)
            .centerCrop()
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(first_image!!)
    }

    private fun hideDownloadUI() {
        button_download_direct!!.isEnabled = false

        first_image!!.setImageResource(0)
        first_image!!.visibility = View.INVISIBLE
    }

    private fun showDownloadUI() {
        button_download_direct!!.isEnabled = true

        first_image!!.visibility = View.VISIBLE
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        // See #choosePhoto with @AfterPermissionGranted
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(
                this,
                listOf(PERMS)
            )
        ) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    companion object {

        private const val TAG = "ImageDemo"
        private const val RC_CHOOSE_PHOTO = 101
        private const val RC_IMAGE_PERMS = 102
        private const val PERMS = Manifest.permission.READ_EXTERNAL_STORAGE
    }
}
