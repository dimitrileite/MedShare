package com.shalomempire.medshare.util


import android.content.Context
import com.firebase.ui.auth.AuthUI
import com.shalomempire.medshare.R
import java.util.*

class ConfigurationUtils private constructor() {

    init {
        throw AssertionError("No instance for you!")
    }

    companion object {

        fun isGoogleMisconfigured(context: Context): Boolean {
            return AuthUI.UNCONFIGURED_CONFIG_VALUE.equals(
                context.getString(R.string.default_web_client_id)
            )
        }

        fun isFacebookMisconfigured(context: Context): Boolean {
            return AuthUI.UNCONFIGURED_CONFIG_VALUE.equals(
                context.getString(R.string.facebook_application_id)
            )
        }

        fun isTwitterMisconfigured(context: Context): Boolean {
            val twitterConfigs = Arrays.asList(
                context.getString(R.string.twitter_consumer_key),
                context.getString(R.string.twitter_consumer_secret)
            )

            return twitterConfigs.contains(AuthUI.UNCONFIGURED_CONFIG_VALUE)
        }

        fun isGitHubMisconfigured(context: Context): Boolean {
            val gitHubConfigs = Arrays.asList(
                context.getString(R.string.firebase_web_host),
                context.getString(R.string.github_client_id),
                context.getString(R.string.github_client_secret)
            )

            return gitHubConfigs.contains(AuthUI.UNCONFIGURED_CONFIG_VALUE)
        }

        fun getConfiguredProviders(context: Context): List<AuthUI.IdpConfig> {
            val providers = ArrayList<AuthUI.IdpConfig>()
            providers.add(AuthUI.IdpConfig.EmailBuilder().build())
            providers.add(AuthUI.IdpConfig.PhoneBuilder().build())

            if (!isGoogleMisconfigured(context)) {
                providers.add(AuthUI.IdpConfig.GoogleBuilder().build())
            }

            if (!isFacebookMisconfigured(context)) {
                providers.add(AuthUI.IdpConfig.FacebookBuilder().build())
            }

            if (!isTwitterMisconfigured(context)) {
                providers.add(AuthUI.IdpConfig.TwitterBuilder().build())
            }

            if (!isGitHubMisconfigured(context)) {
                providers.add(AuthUI.IdpConfig.GitHubBuilder().build())
            }

            return providers
        }
    }
}
