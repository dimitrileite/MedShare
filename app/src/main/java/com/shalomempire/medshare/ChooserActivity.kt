/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shalomempire.medshare

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shalomempire.medshare.auth.AnonymousUpgradeActivity
import com.shalomempire.medshare.auth.AuthUiActivity
import com.shalomempire.medshare.database.firestore.FirestoreChatActivity
import com.shalomempire.medshare.database.firestore.FirestorePagingActivity
import com.shalomempire.medshare.database.realtime.RealtimeDbChatActivity
import com.shalomempire.medshare.storage.ImageActivity
import kotlinx.android.synthetic.main.activity_chooser.*

class ChooserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chooser)

        activities.layoutManager = LinearLayoutManager(this)
        activities.adapter = ActivityChooserAdapter()
        activities.setHasFixedSize(true)
    }

    private class ActivityChooserAdapter : RecyclerView.Adapter<ActivityStarterHolder>() {

        override fun getItemCount(): Int {
            return CLASSES.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActivityStarterHolder {
            return ActivityStarterHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.activity_chooser_item, parent, false)
            )
        }

        override fun onBindViewHolder(holder: ActivityStarterHolder, position: Int) {
            holder.bind(CLASSES[position], DESCRIPTION_NAMES[position], DESCRIPTION_IDS[position])
        }

        companion object {
            private val CLASSES = arrayOf<Class<*>>(
                AuthUiActivity::class.java,
                AnonymousUpgradeActivity::class.java,
                FirestoreChatActivity::class.java,
                FirestorePagingActivity::class.java,
                RealtimeDbChatActivity::class.java,
                ImageActivity::class.java
            )

            private val DESCRIPTION_NAMES = intArrayOf(
                R.string.title_auth_activity,
                R.string.title_anonymous_upgrade,
                R.string.title_firestore_activity,
                R.string.title_firestore_paging_activity,
                R.string.title_realtime_database_activity,
                R.string.title_storage_activity
            )

            private val DESCRIPTION_IDS = intArrayOf(
                R.string.desc_auth,
                R.string.desc_anonymous_upgrade,
                R.string.desc_firestore,
                R.string.desc_firestore_paging,
                R.string.desc_realtime_database,
                R.string.desc_storage
            )
        }
    }

    private class ActivityStarterHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val mTitle: TextView = itemView.findViewById(R.id.text1)
        private val mDescription: TextView = itemView.findViewById(R.id.text2)
        private var mStarterClass: Class<*>? = null

        internal fun bind(aClass: Class<*>, @StringRes name: Int, @StringRes description: Int) {
            mStarterClass = aClass

            mTitle.setText(name)
            mDescription.setText(description)
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            itemView.context.startActivity(Intent(itemView.context, mStarterClass))
        }
    }


}
