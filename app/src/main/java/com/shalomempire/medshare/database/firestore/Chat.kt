package com.shalomempire.medshare.database.firestore

import com.google.firebase.firestore.IgnoreExtraProperties
import com.google.firebase.firestore.ServerTimestamp
import com.shalomempire.medshare.database.AbstractChat
import java.util.*

@IgnoreExtraProperties
class Chat : AbstractChat {
    override var name: String? = null
    override var message: String? = null
    override var uid: String? = null
    @get:ServerTimestamp
    var timestamp: Date? = null

    constructor() {
        // Needed for Firebase
    }

    constructor(name: String?, message: String?, uid: String) {
        this.name = name
        this.message = message
        this.uid = uid
    }

    override fun equals(obj: Any?): Boolean {
        if (this === obj) return true
        if (obj == null || javaClass != obj.javaClass) return false

        val chat = obj as Chat?

        return (timestamp == chat!!.timestamp
                && uid == chat.uid
                && (if (name == null) chat.name == null else name == chat.name)
                && if (message == null) chat.message == null else message == chat.message)
    }

    override fun hashCode(): Int {
        var result = if (name == null) 0 else name!!.hashCode()
        result = 31 * result + if (message == null) 0 else message!!.hashCode()
        result = 31 * result + uid!!.hashCode()
        result = 31 * result + timestamp!!.hashCode()
        return result
    }

    override fun toString(): String {
        return "Chat{" +
                "mName='" + name + '\''.toString() +
                ", mMessage='" + message + '\''.toString() +
                ", mUid='" + uid + '\''.toString() +
                ", mTimestamp=" + timestamp +
                '}'.toString()
    }
}
