package com.shalomempire.medshare.database.realtime

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.firebase.ui.auth.util.ui.ImeHelper
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.shalomempire.medshare.R
import com.shalomempire.medshare.database.ChatHolder
import com.shalomempire.medshare.util.SignInResultNotifier
import kotlinx.android.synthetic.main.activity_chat.*

/**
 * Class demonstrating how to setup a [RecyclerView] with an adapter while taking sign-in
 * states into consideration. Also demonstrates adding data to a ref and then reading it back using
 * the [FirebaseRecyclerAdapter] to build a simple chat app.
 *
 *
 * For a general intro to the RecyclerView, see [Creating
 * Lists](https://developer.android.com/training/material/lists-cards.html).
 */
open class RealtimeDbChatActivity : AppCompatActivity(), FirebaseAuth.AuthStateListener, View.OnClickListener {

    private val isSignedIn: Boolean
        get() = FirebaseAuth.getInstance().currentUser != null

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        messagesList!!.setHasFixedSize(true)
        messagesList!!.layoutManager = LinearLayoutManager(this)

        ImeHelper.setImeOnDoneListener(messageEdit) { onSendClick() }

        sendButton.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.sendButton -> onSendClick()
        }
    }

    override fun onStart() {
        super.onStart()
        if (isSignedIn) {
            attachRecyclerViewAdapter()
        }
        FirebaseAuth.getInstance().addAuthStateListener(this)
    }

    override fun onStop() {
        super.onStop()
        FirebaseAuth.getInstance().removeAuthStateListener(this)
    }

    override fun onAuthStateChanged(auth: FirebaseAuth) {
        sendButton.isEnabled = isSignedIn
        messageEdit.isEnabled = isSignedIn

        if (isSignedIn) {
            attachRecyclerViewAdapter()
        } else {
            Toast.makeText(this, R.string.signing_in, Toast.LENGTH_SHORT).show()
            auth.signInAnonymously().addOnCompleteListener(SignInResultNotifier(this))
        }
    }

    private fun attachRecyclerViewAdapter() {
        val adapter = newAdapter()

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                messagesList!!.smoothScrollToPosition(adapter.itemCount)
            }
        })

        messagesList!!.adapter = adapter
    }

    fun onSendClick() {
        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        val name = "User " + uid.substring(0, 6)

        onAddMessage(Chat(name, messageEdit.text.toString(), uid))

        messageEdit.setText("")
    }

    private fun newAdapter(): Adapter<ChatHolder> {
        val options = FirebaseRecyclerOptions.Builder<Chat>()
            .setQuery(sChatQuery, Chat::class.java)
            .setLifecycleOwner(this)
            .build()

        return object : FirebaseRecyclerAdapter<Chat, ChatHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatHolder {
                return ChatHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.message, parent, false)
                )
            }

            override fun onBindViewHolder(holder: ChatHolder, position: Int, model: Chat) {
                holder.bind(model)
            }

            override fun onDataChanged() {
                // If there are no chat messages, show a view that invites the user to add a message.
                emptyTextView.visibility = if (itemCount == 0) View.VISIBLE else View.GONE
            }
        }
    }

    private fun onAddMessage(chat: Chat) {
        // sChatQuery.ref.push().setValue(chat) { error, reference ->
        sChatQuery.ref.push().setValue(chat) { error, _ ->
            if (error != null) {
                Log.e(TAG, "Failed to write message", error.toException())
            }
        }
    }

    companion object {
        private const val TAG = "RealtimeDatabaseDemo"

        /**
         * Get the last 50 chat messages.
         */
        val sChatQuery = FirebaseDatabase.getInstance().reference.child("chats").limitToLast(50)
    }
}
