package com.shalomempire.medshare.database.realtime

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.shalomempire.medshare.R
import com.shalomempire.medshare.database.ChatHolder
import kotlinx.android.synthetic.main.activity_chat.*

class RealtimeDbChatIndexActivity : RealtimeDbChatActivity() {

    private var mChatIndicesRef: DatabaseReference? = null

    private fun newAdapter(): FirebaseRecyclerAdapter<Chat, ChatHolder> {
        mChatIndicesRef = FirebaseDatabase.getInstance()
            .reference
            .child("chatIndices")
            .child(FirebaseAuth.getInstance().currentUser!!.uid)

        val options = FirebaseRecyclerOptions.Builder<Chat>()
            .setIndexedQuery(
                mChatIndicesRef!!.limitToFirst(50), sChatQuery.ref, Chat::class.java
            )
            .setLifecycleOwner(this)
            .build()

        return object : FirebaseRecyclerAdapter<Chat, ChatHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatHolder {
                return ChatHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.message, parent, false)
                )
            }

            override fun onBindViewHolder(holder: ChatHolder, position: Int, model: Chat) {
                holder.bind(model)
            }

            override fun onDataChanged() {
                // If there are no chat messages, show a view that invites the user to add a message.
                emptyTextView.visibility = if (itemCount == 0) View.VISIBLE else View.GONE
            }
        }
    }

    private fun onAddMessage(chat: Chat) {
        val chatRef = sChatQuery.ref.push()
        mChatIndicesRef!!.child(chatRef.key!!).setValue(true)
        chatRef.setValue(chat)
    }
}
