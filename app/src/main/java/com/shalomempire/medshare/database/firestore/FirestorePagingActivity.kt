package com.shalomempire.medshare.database.firestore

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.Locale

import com.firebase.ui.firestore.paging.FirestorePagingAdapter
import com.firebase.ui.firestore.paging.FirestorePagingOptions
import com.firebase.ui.firestore.paging.LoadingState

import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.OnCompleteListener

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.WriteBatch

import com.shalomempire.medshare.R

import kotlinx.android.synthetic.main.activity_firestore_paging.*
import kotlinx.android.synthetic.main.item_item.*
import android.widget.TextView
import androidx.annotation.StringRes
import kotlinx.android.synthetic.main.item_item.view.*


class FirestorePagingActivity : AppCompatActivity() {

    private var mFirestore: FirebaseFirestore? = null
    private var mItemsCollection: CollectionReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firestore_paging)

        mFirestore = FirebaseFirestore.getInstance()
        mItemsCollection = mFirestore!!.collection("items")

        setUpAdapter()
    }

    private fun setUpAdapter() {
        val baseQuery = mItemsCollection!!.orderBy("value", Query.Direction.ASCENDING)

        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPrefetchDistance(10)
            .setPageSize(20)
            .build()

        val options = FirestorePagingOptions.Builder<Item>()
            .setLifecycleOwner(this)
            .setQuery(baseQuery, config, Item::class.java)
            .build()

        val adapter = object : FirestorePagingAdapter<Item, ItemViewHolder>(options) {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): ItemViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_item, parent, false)
                return ItemViewHolder(view)
            }

            override fun onBindViewHolder(
                holder: ItemViewHolder,
                position: Int,
                model: Item
            ) {
                holder.bind(model)
            }

            override fun onLoadingStateChanged(state: LoadingState) {
                when (state) {
                    LoadingState.LOADING_INITIAL, LoadingState.LOADING_MORE -> paging_loading.visibility = View.VISIBLE
                    LoadingState.LOADED -> paging_loading.visibility = View.GONE
                    LoadingState.FINISHED -> {
                        paging_loading.visibility = View.GONE
                        showToast("Reached end of data set.")
                    }
                    LoadingState.ERROR -> {
                        showToast("An error occurred.")
                        retry()
                    }
                }
            }
        }

        paging_recycler.layoutManager = LinearLayoutManager(this)
        paging_recycler.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_firestore_paging, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.item_add_data) {
            showToast("Adding data...")
            createItems().addOnCompleteListener(
                this
            ) { task ->
                if (task.isSuccessful) {
                    showToast("Data added.")
                } else {
                    Log.w(TAG, "addData", task.exception)
                    showToast("Error adding data.")
                }
            }

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createItems(): Task<Void> {
        val writeBatch = mFirestore!!.batch()

        for (i in 0..249) {
            val title = "Item $i"

            val id = String.format(Locale.getDefault(), "item_%03d", i)
            val item = Item(title, i)

            writeBatch.set(mItemsCollection!!.document(id), item)
        }

        return writeBatch.commit()
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    data class Item(
        var text: String,
        var value: Int){
        constructor() : this("", -1)
    }

    private class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val mTitle: TextView = itemView.findViewById(R.id.item_text)
        private val mDescription: TextView = itemView.findViewById(R.id.item_value)

        internal fun bind(item: Item?) {
            mTitle.text = item!!.text
            mDescription.text = item.text
        }
    }

    companion object {

        private const val TAG = "FirestorePagingActivity"
    }

}
